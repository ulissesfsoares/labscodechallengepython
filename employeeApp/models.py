from django.db import models

# Create your models here.
class Employee(models.Model):
    name = models.CharField(max_length=255, null=False)
    email = models.CharField(primary_key=True, max_length=255, null=False)
    department = models.CharField(max_length=50, null=False)