from .models import Employee
from rest_framework import serializers, routers, viewsets

class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ('name', 'email', 'department')
"""       
class EmployeeViewSet(viewSets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    
router = routers.DefaultRouter()
router.register(r'employee', EmployeeViewSet)

urlpatterns = router.urls
"""
    