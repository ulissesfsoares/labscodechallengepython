from .models import Employee
from django.http import Http404

from employeeApp.serializers import EmployeeSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class EmployeeList(APIView):
    def get(self, request, format=None):
        Employees = Employee.objects.all()
        serializer = EmployeeSerializer(Employees, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = EmployeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, format=None):
        Employee = self.get_object(pk)
        Employee.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class EmployeeDetail(APIView):
    def get_object(self, email):
        try:
            return Employee.objects.get(email=email)
        except Employee.DoesNotExist:
            raise Http404

    def get(self, request, email, format=None):
        Employee = self.get_object(self.kwargs['email'])
        Employee = EmployeeSerializer(Employee)
        return Response(Employee.data)

    def put(self, request, email, format=None):
        Employee = self.get_object(self.kwargs['email'])
        serializer = EmployeeSerializer(Employee, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, email, format=None):
        Employee = self.get_object(self.kwargs['email'])
        Employee.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
