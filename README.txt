Projeto de code challenge por Ulisses Soares.

O projeto esta configurado para usar um banco mysql com os dados de conexão:  

Server=localhost
Port=3306
Database=labs_challenge_db
Uid=labs_user;
Password=p123456w;

A interface gráfica esta disponivel em http://localhost:8000/admin/employeeApp
A URL base para a API é http://localhost:8000/employee

Ao utilizar outras configurações de banco atualizar setting.py e fazer a migração